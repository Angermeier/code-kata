package berlinclock;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.io.Closeable;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainController implements Closeable {
    @FXML
    TextField time;
    @FXML
    TextArea timeArea;
    @FXML
    CheckBox checkTime;
    @FXML
    Circle circleSeconds;
    @FXML
    Rectangle rectH1, rectH2, rectH3, rectH4, rectH5, rectH6, rectH7, rectH8;
    @FXML
    Rectangle rectM1, rectM2, rectM3, rectM4, rectM5, rectM6, rectM7, rectM8,
            rectM9, rectM10, rectM11, rectM12, rectM13, rectM14, rectM15;
    private StringProperty timeProperty;
    private Timer timer = new Timer();
    private TimerTask timerTask;
    private BerlinClock berlinClock = new BerlinClock();
    private BooleanProperty[][] booleans;

    @FXML
    private void initialize() {
        booleans = new BooleanProperty[][]
                {{circleSeconds.visibleProperty()},
                        {rectH1.visibleProperty(), rectH2.visibleProperty(), rectH3.visibleProperty(), rectH4.visibleProperty()},
                        {rectH5.visibleProperty(), rectH6.visibleProperty(), rectH7.visibleProperty(), rectH8.visibleProperty()},
                        {rectM1.visibleProperty(), rectM2.visibleProperty(), rectM3.visibleProperty(), rectM4.visibleProperty(), rectM5.visibleProperty(), rectM6.visibleProperty(), rectM7.visibleProperty(), rectM8.visibleProperty(), rectM9.visibleProperty(), rectM10.visibleProperty(), rectM11.visibleProperty()},
                        {rectM12.visibleProperty(), rectM13.visibleProperty(), rectM14.visibleProperty(), rectM15.visibleProperty()}};
        this.selectSystemTime(null);
        timeProperty = time.textProperty();
        timeProperty.addListener((observable, oldValue, newValue) -> {
                    if (newValue.matches("^([0-9]{2}:){2}[0-9]{2}$")) {
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                berlinClock.parseTime(newValue);
                            }
                        }, 0);
                    }
                }
        );
        berlinClock.addListener((observable, oldvalue, newValue) -> {
            timeArea.textProperty().setValue(newValue);
            boolean[][] timeArray = berlinClock.getBools();
            for (int i = 0; i < timeArray.length; i++) {
                boolean[] timea = timeArray[i];
                for (int j = 0; j < timea.length; j++) {
                    booleans[i][j].set(timeArray[i][j]);
                }
            }
        });

    }

    @FXML
    private synchronized void selectSystemTime(ActionEvent ae) {
        boolean useSystemTime = checkTime.isSelected();
        time.setEditable(!useSystemTime);
        if (useSystemTime) {
            timerTask = getNewTimerTask();
            timer.schedule(timerTask, 0, 300);
        } else {
            timerTask.cancel();
        }
    }

    private TimerTask getNewTimerTask() {
        return new TimerTask() {
            public void run() {
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
                timeProperty.setValue(sdfDate.format(new Date()));
            }
        };
    }

    @Override
    public void close() {
        timer.cancel();
    }
}
