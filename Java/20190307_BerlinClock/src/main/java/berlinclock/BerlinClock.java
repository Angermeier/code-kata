package berlinclock;

import javafx.beans.property.SimpleObjectProperty;

public class BerlinClock extends SimpleObjectProperty<String> {
    private boolean[][] timeArray;
    private final char[][] timeStrings = {{'Y'},
            {'R', 'R', 'R', 'R'},
            {'R', 'R', 'R', 'R'},
            {'Y', 'Y', 'R', 'Y', 'Y', 'R', 'Y', 'Y', 'R', 'Y', 'Y'},
            {'Y', 'Y', 'Y', 'Y'}};

    public boolean[][] getBools() {
        return timeArray;
    }

    public void parseTime(String timeString) {
        String[] times = timeString.split(":");
        resetTimeArray();
        parseSeconds(times[2], timeArray[0]);
        parseMinuteHours(times[0], timeArray[1], timeArray[2]);
        parseMinuteHours(times[1], timeArray[3], timeArray[4]);
        this.set(this.toString());
    }

    private void resetTimeArray() {
        timeArray = new boolean[][]{{false}, {false, false, false, false}, {false, false, false, false},
                {false, false, false, false, false, false, false, false, false, false, false}, {false, false, false, false}};
    }

    private void parseMinuteHours(String time, boolean[] bools1, boolean[] bools2) {
        int t = Integer.parseInt(time);
        for (int i = 0; i < t / 5; i++) {
            bools1[i] = true;
        }
        for (int i = 0; i < t % 5; i++) {
            bools2[i] = true;
        }
    }

    private void parseSeconds(String secondsString, boolean[] secondsArray) {
        secondsArray[0] = !(Integer.parseInt(secondsString) % 2 == 0);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < timeArray.length; i++) {
            boolean[] timea = timeArray[i];
            for (int j = 0; j < timea.length; j++) {
                sb.append(timea[j] ? timeStrings[i][j] : '0');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
