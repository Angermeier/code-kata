package berlinclock;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.Closeable;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MainApp extends Application {
    public static void main(String... args) {
        launch(args);
    }

    List<Closeable> controllers = new LinkedList<>();

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Berlin o'Clock");

        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/MainController.fxml"));
            AnchorPane rootLayout = loader.load();
            controllers.add(loader.getController());

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            scene.getStylesheets().add("/style/berlin.css");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() throws IOException {
        for (Closeable controller : controllers) {
            controller.close();
        }
    }
}
