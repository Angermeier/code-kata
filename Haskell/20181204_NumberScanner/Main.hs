module Main where

main :: IO ()
main = do
    numbers <- readFile "numbers.txt"
    (putStrLn . translateNumbers . extractNumbers . splitString . stripNewLines) numbers

stripNewLines :: String -> String
stripNewLines xs = [ x | x <- xs, x /= '\n']

splitString :: String -> [String]
splitString xs
  | null xs = []
  | otherwise = take 3 xs:splitString (drop 3 xs)

extractNumbers :: [String] -> [String]
extractNumbers [] = []
extractNumbers xs = (takeByIndex 0 xs ++ takeByIndex 1 xs ++ takeByIndex 2 xs):
                    extractNumbers (removeByIndex 0 (removeByIndex 1 (removeByIndex 2 xs)))

takeByIndex :: Int -> [String] -> String
takeByIndex i xs = xs !! max 0 (getIndex i xs)

removeByIndex :: Int -> [String] -> [String]
removeByIndex i xs = take (getIndex i xs) xs ++ drop (1 + getIndex i xs) xs

getIndex :: Int -> [String] -> Int
getIndex i xs 
  | length xs `mod` 3 == 0 = length xs `div` 3 * i
  | otherwise = (length xs + (3 - length xs `mod` 3)) `div` 3 * i
 
translateNumbers :: [String] -> String
translateNumbers xs = [translateNumber x | x <- xs]

translateNumber :: String -> Char
translateNumber " _ | ||_|" = '0'
translateNumber "     |  |" = '1'
translateNumber " _  _||_ " = '2'
translateNumber " _  _| _|" = '3'
translateNumber "   |_|  |" = '4'
translateNumber " _ |_  _|" = '5'
translateNumber " _ |_ |_|" = '6'
translateNumber " _   |  |" = '7'
translateNumber " _ |_||_|" = '8'
translateNumber " _ |_| _|" = '9'
translateNumber _ = error "NaN"
