module Main where

import System.Environment ( getArgs )

main :: IO ()
main = do
    s <- getArgs
    (putStrLn . parseTime . head) s

parseTime :: String -> String
parseTime time = parseSeconds (take 2 (drop 6 time)) ++ "\n" ++ 
                 parseMinutesHours (read (take 2 time)) 4 "R" "R" ++ "\n" ++
                 parseMinutesHours (read (take 2 (drop 3 time))) 11 "YYR" "Y"

parseSeconds :: String -> String
parseSeconds s
  | even (read s) = "Y"
  | otherwise = "O"

parseMinutesHours :: Int -> Int -> String -> String -> String
parseMinutesHours time i cycle1 cycle2 = take i (take (time `div` 5) (cycle cycle1) ++ repeat 'O') ++ "\n" ++ 
                                         take 4 (take (time `mod` 5) (cycle cycle2) ++ repeat 'O')
