module Main where

main :: IO ()
main = print doit

doit :: [String]
doit = [ translateNumber x | x <- [1..100]]

translateNumber :: Int -> String
translateNumber x 
  | x `mod` 3 == 0 && x `mod` 5 == 0 = "Boom"
  | x `mod` 3 == 0 = "Fizz"
  | x `mod` 5 == 0 = "Buzz"
  | otherwise = show x