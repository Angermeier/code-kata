module Main where

import System.Environment ( getArgs )
import Data.List ( sort )
import qualified Data.Map     as Map
import qualified Data.Text    as Text
import qualified Data.Text.IO as Text
import Data.Maybe ( isNothing )

main :: IO ()
main = do
    args <- getArgs
    words <- fmap Text.lines (Text.readFile "wordlistutf8.txt")
    printList (parse args words)

printList :: [String] -> IO ()
printList [x] = putStrLn x
printList (x:xs) = do 
                  putStrLn x
                  printList xs

parse :: [String] -> [Text.Text] -> [String]
parse args words
  | head args == "p" = palindrome (tail args)
  | head args == "a" = anagram (tail args)
  | head args == "ma" = moreAnagrams words
  | otherwise = error (show args)

palindrome :: [String] -> [String]
palindrome xs 
  | head xs == reverse (head xs) = [head xs ++ " is a Palindrome!"]
  | otherwise = [head xs ++ " is NOT a Palindrome!"]

anagram :: [String] -> [String]
anagram xs
  | length [ x | x <- xs, sort x == sort (head xs)] == length xs = [show xs ++ " are all anagrams"]
  | otherwise = [show xs ++ " aren't all anagrams"]

moreAnagrams :: [Text.Text] -> [String]
moreAnagrams xs = [show x | x <- Map.toList (rec xs Map.empty)]

rec :: [Text.Text] -> Map.Map String [Text.Text] -> Map.Map String [Text.Text]
rec [x] mp = insertTxt x mp
rec (x:xs) mp = Map.unionWith (++) (insertTxt x mp) (rec xs mp)

insertTxt :: Text.Text -> Map.Map String [Text.Text] -> Map.Map String [Text.Text]
insertTxt txt mp
  | isNothing (Map.lookup (sort (Text.unpack txt)) mp) = Map.insert (sort (Text.unpack txt)) [txt] mp
  | otherwise = Map.adjust (++ [txt]) (sort (Text.unpack  txt)) mp
