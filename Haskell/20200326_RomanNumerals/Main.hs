module Main where

import System.Environment ( getArgs )
import Data.Map as M ( fromList, lookup )
import Data.Maybe ( fromMaybe )

main :: IO ()
main = do
        args <- getArgs
        (putStrLn . select) args

select :: [String] -> String 
select args
  | head args == "d" = decodeRomanNumeral (last args)
  | otherwise = encodeToRomanNumeral (read (last args))

decodeRomanNumeral :: String -> String 
decodeRomanNumeral s = show (decode s)

encodeToRomanNumeral :: Int -> String 
encodeToRomanNumeral i = encode i "MDCLXVI"

encode :: Int -> String -> String 
encode i (c:cs)
  | c == 'M' = let (s, rem) = encode' i c '-' 'C' in s ++ encode rem cs
  | c == 'D' = let (s, rem) = encode' i c 'M' 'L' in s ++ encode rem cs
  | c == 'C' = let (s, rem) = encode' i c 'D' 'X' in s ++ encode rem cs
  | c == 'L' = let (s, rem) = encode' i c 'C' 'V' in s ++ encode rem cs
  | c == 'X' = let (s, rem) = encode' i c 'L' 'I' in s ++ encode rem cs
  | c == 'V' = let (s, rem) = encode' i c 'X' '-' in s ++ encode rem cs
  | c == 'I' = let (s, rem) = encode' i c 'V' '-' in s
  where
    encode' i c = encode'' i (getValue c) c
    encode'' reminder num char charup char2down
      | reminder <= 0 = ("", 0)
      | reminder `div` num == 4 = ([char, charup] , reminder - 4 * num)
      | tenthTest reminder num = (encode''' reminder num char ++ [char2down, char], calcReminder reminder num - 9 * (num `div` 10))
      | otherwise = (encode''' reminder num char, calcReminder reminder num)
      where
        encode''' reminder num = replicate (reminder `div` num)
        calcReminder reminder num = reminder - (reminder `div` num) * num
        tenthTest reminder num = num `div` 10 /= 0 && reminder `mod` num `div` (num `div` 10) == 9

decode :: String -> Int
decode s = sum (fix [getValue x | x <- s])
  where
    fix [a,b]
      | a < b = [-a,b]
      | otherwise = [a,b]
    fix (a:b:xs)
      | a < b = (-a):fix (b:xs)
      | otherwise = a:fix (b:xs)

getValue :: Char -> Int
getValue c = fromMaybe 0 (M.lookup c m)
  where m = M.fromList [('M', 1000),('D',500),('C',100),('L',50),('X',10),('V',5),('I',1)]
